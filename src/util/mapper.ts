import { TodoDTO } from "src/feature/todo/dto/todo.dto";
import { TodoEntity } from "src/feature/todo/todo.entity";
import { UserDTO } from "src/feature/user/dto/user.dto";
import { UserEntity } from "src/feature/user/user.entity";

export const convertToUserDTO = (data: UserEntity): UserDTO => {
  const {id, username} = data;
  let user: UserDTO = {
    id,
    username
  }
  return user;
}

export const convertToTodoDTO = (data: TodoEntity): TodoDTO => {
  const {id, name, description, isDone, owner, createAt,  doneAt} = data;
  let todo: TodoDTO = {
    id,
    name,
    isDone,
    createAt
  };
  if(isDone) todo.doneAt = doneAt;
  if(description) todo.description = description;
  if(owner) todo.owner = convertToUserDTO(owner);
  return todo;
}