export default () => ({
  port: parseInt(process.env.PORT) || 3000,
  database: {
    type: 'postgres',
    host: process.env.DB_HOST || 'localhost',
    port: 5432,
    username: process.env.DB_USERNAME || 'postgres',
    password: process.env.DB_PASSWORD || 'postgres',
    database: process.env.DB_DATABASE || 'simple_todo_testing',
    entities: ["dist/**/*.entity{.ts,.js}"],
    synchronize: true,
  },
  jwt_expiresIn: process.env.JWT_EXPIRE || '1d',
  jwt_secret: process.env.JWT_SECRET || 'secret',
});