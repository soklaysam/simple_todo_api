import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

const PORT = new ConfigService().get('port');
const GLOBAL_PREFIX = 'api';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix(GLOBAL_PREFIX);
  
  const document = SwaggerModule.createDocument(app, new DocumentBuilder()
    .setTitle('Todo API')
    .setDescription('Just a simple Todo API using NestJS and Postgres')
    .setVersion('1.0')
    .addBearerAuth()
    .build());
  SwaggerModule.setup(GLOBAL_PREFIX, app, document);

  await app.listen(PORT);
}
bootstrap();
