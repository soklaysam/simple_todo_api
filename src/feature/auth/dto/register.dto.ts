import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";

export class RegisterDTO{
  @ApiProperty({example:'admin'})
  @IsNotEmpty()
  @IsString()
  username: string;

  @ApiProperty({example:'1234'})
  @IsNotEmpty()
  @IsString()
  password: string;
}