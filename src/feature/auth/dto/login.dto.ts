import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, isNotEmpty, IsString } from "class-validator";

export class LoginDTO{
  @ApiProperty({example:'admin'})
  @IsNotEmpty()
  @IsString()
  username: string;

  @ApiProperty({example:'1234'})
  @IsNotEmpty()
  @IsString()
  password: string;
}