import { Body, Controller, HttpCode, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { LoginDTO } from './dto/login.dto';
import { RegisterDTO } from './dto/register.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService){}

  @ApiResponse({ status: 201, description: 'Success Register'})
  @ApiResponse({ status: 400, description: 'Username already existed'})
  @Post('register')
  @UsePipes(ValidationPipe)
  async register(@Body() data: RegisterDTO){
    return await this.authService.register(data);
  }

  @ApiResponse({ status: 200, description: 'Success Login'})
  @ApiResponse({ status: 401, description: 'Incorrect Username or Password' })
  @HttpCode(200)
  @Post('login')
  @UsePipes(ValidationPipe)
  async login(@Body() data: LoginDTO){
    return await this.authService.login(data);
  }
}
