import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { LoginDTO } from './dto/login.dto';
import { RegisterDTO } from './dto/register.dto';
import * as bcrypt from 'bcrypt';
import { ConfigService } from '@nestjs/config';
import { CreateUserDTO } from '../user/dto/create.user.dto';
import { UserDTO } from '../user/dto/user.dto';
import { UserEntity } from '../user/user.entity';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private configService: ConfigService,
    private jwtService: JwtService
  ){}

  async register(data: RegisterDTO): Promise<UserDTO>{
    let user: CreateUserDTO = new CreateUserDTO();
    user.username = data.username;
    user.password = await bcrypt.hash(data.password, 7);
    return await this.userService.create(user);
  }

  async login(data: LoginDTO){
    const user: UserEntity = await this.validateUser(data.username);
    if (!user) throw new UnauthorizedException('Incorrect Username or Password');
    const valid = await bcrypt.compare(data.password, user.password);
    if (!valid) throw new UnauthorizedException('Incorrect Password or Password');
    const payload = {
      sub: user.id,
      username: user.username
    }
    const accessToken = this.jwtService.sign(payload, {
      secret: this.configService.get('jwt_secret'), 
      expiresIn: this.configService.get('jwt_expiresIn')
    });
    return {
      id: user.id,
      username: user.username,
      accessToken,
      expiresIn: this.configService.get('jwt_expiresIn')
    }
  }

  async validateUser(username: string){
    return await this.userService.findByUsername(username)
  }
}
