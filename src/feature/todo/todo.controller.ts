import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, Query, Req, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiBearerAuth, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { JWTAuthGuard } from '../auth/jwt-auth.guard';
import { CreateTodoDTO } from './dto/create.todo.dto';
import { UpdateTodoDTO } from './dto/update.todo.dto';
import { TodoService } from './todo.service';
import { Request } from 'express';

@ApiBearerAuth()
@ApiTags('Todo')
@UseGuards(JWTAuthGuard)
@Controller('todo')
export class TodoController {
  constructor(private todoService: TodoService){}

  @ApiResponse({ status: 200 , description: 'Return Todo List'})
  @ApiResponse({ status: 404 , description: 'Not Found'})
  @ApiQuery({name:'username', required: false})
  @Get()
  async findByOwner(@Query('username') username?: string){
    if(!username) return await this.todoService.findAll()
    else return await this.todoService.findByOwner(username);
  }

  @ApiResponse({ status: 200 , description: 'Return Todo'})
  @ApiResponse({ status: 404, description: 'Todo does not existed'})
  @Get(':id')
  async findOne(@Param('id', ParseIntPipe) id: number){
    return await this.todoService.findOne(id);
  }

  @ApiResponse({ status: 201, description: 'Success Create'})
  @Post()
  @UsePipes(ValidationPipe)
  async create(@Req() req: Request, @Body() data: CreateTodoDTO){
    const username = req.user['username'];
    return await this.todoService.create(username, data);
  }

  @ApiResponse({ status: 200, description: 'Success Update'})
  @ApiResponse({ status: 404, description: 'Todo does not existed'})
  @Put(':id')
  @UsePipes(ValidationPipe)
  async update(@Param('id', ParseIntPipe) id: number, @Body() data: UpdateTodoDTO){
    return await this.todoService.update(id, data);
  }

  @ApiResponse({ status: 200, description: 'Success Delete'})
  @ApiResponse({ status: 404, description: 'Todo does not existed'})
  @Delete(':id')
  async delete(@Param('id', ParseIntPipe) id: number){
    return await this.todoService.delete(id);
  }
}

