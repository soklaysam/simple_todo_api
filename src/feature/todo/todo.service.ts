import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { convertToTodoDTO } from 'src/util/mapper';
import { Repository } from 'typeorm';
import { UserService } from '../user/user.service';
import { CreateTodoDTO } from './dto/create.todo.dto';
import { TodoDTO } from './dto/todo.dto';
import { UpdateTodoDTO } from './dto/update.todo.dto';
import { TodoEntity } from './todo.entity';

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(TodoEntity) private todoRepo: Repository<TodoEntity>,
    private userService: UserService
  ){}

  async create(username: string, data: CreateTodoDTO): Promise<TodoDTO> {
    const user = await this.userService.findByUsername(username);
    const todo = await this.todoRepo.save({...data, owner: user});
    return convertToTodoDTO(todo);
  }

  async findAll(): Promise<TodoDTO[]>{
    const todos = await this.todoRepo.find({relations: ['owner']});
    return todos.map(todo=>convertToTodoDTO(todo));
  }

  async findByOwner(username: string): Promise<TodoDTO[]>{
    const user = await this.userService.findByUsername(username);
    if(!user) throw new NotFoundException()
    const todos = await this.todoRepo.find({where: {owner: user}});
    return todos.map(todo=>convertToTodoDTO(todo));
  }

  async findOne(id: number): Promise<TodoDTO>{
    const todo = await this.todoRepo.findOne(id, {relations: ['owner']});
    if(!todo) throw new NotFoundException('Todo does not existed');
    return convertToTodoDTO(todo);
  }

  async update(id: number, data: UpdateTodoDTO): Promise<TodoDTO>{
    let todo = await this.todoRepo.findOne(id, {relations: ['owner']});
    if(!todo) throw new NotFoundException('Todo does not existed');
    todo.name = data.name || todo.name;
    todo.description = data.description || todo.description;
    if(data.isDone){
      todo.isDone = true;
      todo.doneAt = new Date();
    }
    else{
      todo.isDone = false;
      todo.doneAt = null;
    }
    await this.todoRepo.save(todo);
    return convertToTodoDTO(todo);
  }

  async delete(id: number): Promise<any>{
    const todo = await this.todoRepo.findOne(id);
    if(!todo) throw new NotFoundException('Todo does not existed');
    await this.todoRepo.remove(todo);
    return 'Success Remove';
  }
}
