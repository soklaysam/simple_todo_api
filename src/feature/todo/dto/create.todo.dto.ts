import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional, IsString } from "class-validator";

export class CreateTodoDTO{
  @ApiProperty({example: 'Learn NestJS'})
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiPropertyOptional({example: 'Learn to connect Postgres with NestJS'})
  @IsString()
  @IsOptional()
  description?: string;
}