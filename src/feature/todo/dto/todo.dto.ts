import { IsBoolean, IsDate, IsNotEmpty, IsOptional, IsString } from "class-validator";
import { UserDTO } from "src/feature/user/dto/user.dto";

export class TodoDTO{
  @IsNotEmpty()
  id: number;

  @IsNotEmpty()
  @IsString()
  name: string;

  @IsString()
  @IsOptional()
  description?: string;

  @IsNotEmpty()
  @IsBoolean()
  isDone: boolean;

  @IsNotEmpty()
  @IsDate()
  createAt: Date;

  @IsDate()
  @IsOptional()
  doneAt?: Date;

  owner?: UserDTO;
}