import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsOptional, IsBoolean, IsDate, IsNotEmpty, IsString } from "class-validator";
export class UpdateTodoDTO{
  @ApiPropertyOptional({example: 'Build Todo API'})
  @IsString()
  @IsOptional()
  name?: string;

  @ApiPropertyOptional({example: 'Just build a simple one is enough'})
  @IsString()
  @IsOptional()
  description?: string;

  @ApiPropertyOptional({example: true})
  @IsBoolean()
  @IsOptional()
  isDone?: boolean;
}