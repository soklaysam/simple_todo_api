import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { UserEntity } from "../user/user.entity";

@Entity()
export class TodoEntity{
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({nullable: true})
  description: string;

  @Column({default: false})
  isDone: boolean;

  @CreateDateColumn()
  createAt: Date;

  @Column({nullable: true})
  doneAt: Date;

  @ManyToOne(type=>UserEntity, owner=>owner.todos)
  owner: UserEntity;
}