import { Expose } from "class-transformer";
import { IsNotEmpty, IsString } from "class-validator";

export class UserDTO{
  @IsNotEmpty()
  id: number;
  
  @IsNotEmpty()
  @IsString()
  username: string;
}