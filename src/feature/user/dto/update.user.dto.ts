import { IsOptional, IsString } from "class-validator";

export class UpdateUserDTO{
  @IsString()
  username: string;
}