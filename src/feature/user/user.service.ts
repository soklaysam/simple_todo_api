import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { convertToUserDTO } from 'src/util/mapper';
import { Repository } from 'typeorm';
import { CreateUserDTO } from './dto/create.user.dto';
import { UserDTO } from './dto/user.dto';
import { UserEntity } from './user.entity';

@Injectable()
export class UserService {
  constructor(@InjectRepository(UserEntity) private userRepo: Repository<UserEntity>){}

  async create(data: CreateUserDTO): Promise<UserDTO> {
    const user = await this.userRepo.save(data)
      .catch(e=>{throw new BadRequestException('Username already existed')});
    return convertToUserDTO(user);
  }

  async findByUsername(username: string): Promise<any> {
    const user = await this.userRepo.findOne({where: {username}})
    return user;
  }
}
