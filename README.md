## Description

Simple Todo API using NestJS, JWT, Bcrypt, TypeORM, Postgres

## Route

| Route | Method | Description
| ----- | ------ | -----------
| ` /api ` | `GET` | This is Swagger UI with API documentation
| ` /api/auth/register ` | `POST` | Register User
| ` /api/auth/login ` | `POST` | Login User
| ` /api/auth/todo ` | `GET` | Show All Todo / Todo by User
| ` /api/auth/todo ` | `POST` | Create a Todo
| ` /api/auth/todo/:id ` | `GET` | Show a Todo by ID
| ` /api/auth/todo/:id ` | `PUT` | Update a Todo by ID
| ` /api/auth/todo/:id ` | `DELETE` | Delete a Todo by ID

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev
```